/*
 * Firebird Open Source J2ee connector - jdbc driver
 *
 * Distributable under LGPL license.
 * You may obtain a copy of the License at http://www.gnu.org/copyleft/lgpl.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * LGPL License for more details.
 *
 * This file was created by members of the firebird development team.
 * All individual contributions remain the Copyright (C) of those
 * individuals.  Contributors to this file are either listed here or
 * can be obtained from a CVS history command.
 *
 * All rights reserved.
 */
package org.firebirdsql.gds.impl.wire;

import org.firebirdsql.gds.TransactionParameterBuffer;


/**
 * Implementation of the {@link org.firebirdsql.gds.TransactionParameterBuffer}
 * interface. 
 */
public class TransactionParameterBufferImpl extends ParameterBufferBase
        implements TransactionParameterBuffer {

    /* (non-Javadoc)
     * @see org.firebirdsql.gds.TransactionParameterBuffer#deepCopy()
     */
    public TransactionParameterBuffer deepCopy() {
        TransactionParameterBufferImpl result = new TransactionParameterBufferImpl();
        
        result.getArgumentsList().addAll( this.getArgumentsList() );
        
        return result;
    }

}
