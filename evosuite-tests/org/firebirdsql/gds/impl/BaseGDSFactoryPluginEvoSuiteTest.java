/*
 * This file was automatically generated by EvoSuite
 */

package org.firebirdsql.gds.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import org.firebirdsql.gds.GDSException;
import org.firebirdsql.gds.impl.jni.EmbeddedGDSFactoryPlugin;
import org.firebirdsql.gds.impl.jni.NativeGDSFactoryPlugin;
import org.firebirdsql.gds.impl.wire.WireGDSFactoryPlugin;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class BaseGDSFactoryPluginEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      EmbeddedGDSFactoryPlugin embeddedGDSFactoryPlugin0 = new EmbeddedGDSFactoryPlugin();
      Class<?> class0 = embeddedGDSFactoryPlugin0.getConnectionClass();
      assertEquals(false, class0.isEnum());
  }

  @Test
  public void test1()  throws Throwable  {
      NativeGDSFactoryPlugin nativeGDSFactoryPlugin0 = new NativeGDSFactoryPlugin();
      String string0 = nativeGDSFactoryPlugin0.getDefaultProtocol();
      assertEquals("jdbc:firebirdsql:native:", string0);
  }

  @Test
  public void test2()  throws Throwable  {
      WireGDSFactoryPlugin wireGDSFactoryPlugin0 = new WireGDSFactoryPlugin();
      int int0 = wireGDSFactoryPlugin0.hashCode();
      assertEquals(1073827593, int0);
  }

  @Test
  public void test3()  throws Throwable  {
      NativeGDSFactoryPlugin nativeGDSFactoryPlugin0 = new NativeGDSFactoryPlugin();
      // Undeclared exception!
      try {
        nativeGDSFactoryPlugin0.getDatabasePath("dwf&ng4F$");
        fail("Expecting exception: IllegalArgumentException");
      } catch(IllegalArgumentException e) {
        /*
         * Incorrect JDBC protocol handling: dwf&ng4F$
         */
      }
  }

  @Test
  public void test4()  throws Throwable  {
      NativeGDSFactoryPlugin nativeGDSFactoryPlugin0 = new NativeGDSFactoryPlugin();
      String string0 = nativeGDSFactoryPlugin0.getDatabasePath("jdbc:firebirdsql:native:");
      assertNotNull(string0);
      assertEquals("", string0);
  }

  @Test
  public void test5()  throws Throwable  {
      EmbeddedGDSFactoryPlugin embeddedGDSFactoryPlugin0 = new EmbeddedGDSFactoryPlugin();
      boolean boolean0 = embeddedGDSFactoryPlugin0.equals((Object) "");
      assertEquals(false, boolean0);
  }

  @Test
  public void test6()  throws Throwable  {
      NativeGDSFactoryPlugin nativeGDSFactoryPlugin0 = new NativeGDSFactoryPlugin();
      boolean boolean0 = nativeGDSFactoryPlugin0.equals((Object) null);
      assertEquals(false, boolean0);
  }
}
