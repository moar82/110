/*
 * This file was automatically generated by EvoSuite
 */

package org.firebirdsql.gds.impl.jni;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import java.util.List;
import org.firebirdsql.gds.GDSException;
import org.firebirdsql.gds.impl.jni.isc_svc_handle_impl;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class isc_svc_handle_implEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      isc_svc_handle_impl isc_svc_handle_impl0 = new isc_svc_handle_impl();
      isc_svc_handle_impl0.clearWarnings();
      assertEquals(false, isc_svc_handle_impl0.isValid());
      assertEquals(true, isc_svc_handle_impl0.isNotValid());
  }

  @Test
  public void test1()  throws Throwable  {
      isc_svc_handle_impl isc_svc_handle_impl0 = new isc_svc_handle_impl();
      isc_svc_handle_impl0.addWarning((GDSException) null);
      assertEquals(true, isc_svc_handle_impl0.isNotValid());
      assertEquals(false, isc_svc_handle_impl0.isValid());
  }

  @Test
  public void test2()  throws Throwable  {
      isc_svc_handle_impl isc_svc_handle_impl0 = new isc_svc_handle_impl();
      int int0 = isc_svc_handle_impl0.getHandle();
      assertEquals(0, int0);
  }

  @Test
  public void test3()  throws Throwable  {
      isc_svc_handle_impl isc_svc_handle_impl0 = new isc_svc_handle_impl();
      List<GDSException> list0 = isc_svc_handle_impl0.getWarnings();
      assertEquals(true, isc_svc_handle_impl0.isNotValid());
      assertNotNull(list0);
      assertEquals(0, isc_svc_handle_impl0.getHandle());
  }

  @Test
  public void test4()  throws Throwable  {
      isc_svc_handle_impl isc_svc_handle_impl0 = new isc_svc_handle_impl();
      boolean boolean0 = isc_svc_handle_impl0.isNotValid();
      assertEquals(true, boolean0);
  }

  @Test
  public void test5()  throws Throwable  {
      isc_svc_handle_impl isc_svc_handle_impl0 = new isc_svc_handle_impl();
      isc_svc_handle_impl0.setHandle((-1546));
      boolean boolean0 = isc_svc_handle_impl0.isNotValid();
      assertEquals(-1546, isc_svc_handle_impl0.getHandle());
      assertEquals(false, boolean0);
  }
}
