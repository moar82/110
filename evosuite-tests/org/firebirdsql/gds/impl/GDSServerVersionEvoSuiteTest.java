/*
 * This file was automatically generated by EvoSuite
 */

package org.firebirdsql.gds.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import org.firebirdsql.gds.impl.GDSServerVersion;
import org.firebirdsql.gds.impl.GDSServerVersionException;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class GDSServerVersionEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      // Undeclared exception!
      try {
        GDSServerVersion.parseRawVersion("00-00.0.0.0 \u0000");
        fail("Expecting exception: IllegalStateException");
      } catch(IllegalStateException e) {
        /*
         * No match found
         */
      }
  }

  @Test
  public void test1()  throws Throwable  {
      try {
        GDSServerVersion.parseRawVersion("{VgVg?9No8");
        fail("Expecting exception: GDSServerVersionException");
      } catch(GDSServerVersionException e) {
        /*
         * information type inappropriate for object specified
         * Version string does not match expected format
         * Expected engine version format: <platform>-<type><major version>.<minor version>.<variant>.<build number> <server name>
         */
      }
  }
}
