/*
 * This file was automatically generated by EvoSuite
 */

package org.firebirdsql.jdbc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import org.firebirdsql.jdbc.FBCachedBlob;
import org.firebirdsql.jdbc.FBDriverNotCapableException;
import org.firebirdsql.jdbc.FBSQLException;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class FBCachedBlobEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      byte[] byteArray0 = new byte[2];
      FBCachedBlob fBCachedBlob0 = new FBCachedBlob(byteArray0);
      fBCachedBlob0.getSynchronizationObject();
      assertEquals(2L, fBCachedBlob0.length());
  }

  @Test
  public void test1()  throws Throwable  {
      byte[] byteArray0 = new byte[1];
      FBCachedBlob fBCachedBlob0 = new FBCachedBlob(byteArray0);
      try {
        fBCachedBlob0.truncate((long) (byte) (-93));
        fail("Expecting exception: FBDriverNotCapableException");
      } catch(FBDriverNotCapableException e) {
        /*
         * Not yet implemented.
         */
      }
  }

  @Test
  public void test2()  throws Throwable  {
      byte[] byteArray0 = new byte[1];
      FBCachedBlob fBCachedBlob0 = new FBCachedBlob(byteArray0);
      FBCachedBlob fBCachedBlob1 = (FBCachedBlob)fBCachedBlob0.detach();
      assertEquals(1L, fBCachedBlob1.length());
  }

  @Test
  public void test3()  throws Throwable  {
      byte[] byteArray0 = new byte[3];
      FBCachedBlob fBCachedBlob0 = new FBCachedBlob(byteArray0);
      boolean boolean0 = fBCachedBlob0.isSegmented();
      assertEquals(3L, fBCachedBlob0.length());
      assertEquals(false, boolean0);
  }

  @Test
  public void test4()  throws Throwable  {
      byte[] byteArray0 = new byte[4];
      FBCachedBlob fBCachedBlob0 = new FBCachedBlob(byteArray0);
      assertEquals(4L, fBCachedBlob0.length());
      
      fBCachedBlob0.free();
      long long0 = fBCachedBlob0.length();
      assertEquals((-1L), long0);
  }

  @Test
  public void test5()  throws Throwable  {
      byte[] byteArray0 = new byte[4];
      FBCachedBlob fBCachedBlob0 = new FBCachedBlob(byteArray0);
      try {
        fBCachedBlob0.setBytes((long) (byte)0, byteArray0, (int) (byte)26, 2003);
        fail("Expecting exception: FBSQLException");
      } catch(FBSQLException e) {
        /*
         * Blob in auto-commit mode is read-only.
         */
      }
  }

  @Test
  public void test6()  throws Throwable  {
      byte[] byteArray0 = new byte[6];
      FBCachedBlob fBCachedBlob0 = new FBCachedBlob(byteArray0);
      try {
        fBCachedBlob0.position((Blob) fBCachedBlob0, 1630L);
        fail("Expecting exception: FBDriverNotCapableException");
      } catch(FBDriverNotCapableException e) {
        /*
         * Not yet implemented.
         */
      }
  }

  @Test
  public void test7()  throws Throwable  {
      byte[] byteArray0 = new byte[3];
      FBCachedBlob fBCachedBlob0 = new FBCachedBlob(byteArray0);
      try {
        fBCachedBlob0.setBytes((long) (byte)58, byteArray0);
        fail("Expecting exception: FBSQLException");
      } catch(FBSQLException e) {
        /*
         * Blob in auto-commit mode is read-only.
         */
      }
  }

  @Test
  public void test8()  throws Throwable  {
      byte[] byteArray0 = new byte[5];
      FBCachedBlob fBCachedBlob0 = new FBCachedBlob(byteArray0);
      try {
        fBCachedBlob0.position(byteArray0, 2147483647L);
        fail("Expecting exception: FBDriverNotCapableException");
      } catch(FBDriverNotCapableException e) {
        /*
         * Not yet implemented.
         */
      }
  }

  @Test
  public void test9()  throws Throwable  {
      byte[] byteArray0 = new byte[1];
      FBCachedBlob fBCachedBlob0 = new FBCachedBlob(byteArray0);
      try {
        fBCachedBlob0.getBinaryStream(79L, 542L);
        fail("Expecting exception: FBDriverNotCapableException");
      } catch(FBDriverNotCapableException e) {
        /*
         * Not yet implemented.
         */
      }
  }

  @Test
  public void test10()  throws Throwable  {
      byte[] byteArray0 = new byte[3];
      FBCachedBlob fBCachedBlob0 = new FBCachedBlob(byteArray0);
      try {
        fBCachedBlob0.setBinaryStream(0L);
        fail("Expecting exception: FBSQLException");
      } catch(FBSQLException e) {
        /*
         * Blob in auto-commit mode is read-only.
         */
      }
  }

  @Test
  public void test11()  throws Throwable  {
      byte[] byteArray0 = new byte[2];
      FBCachedBlob fBCachedBlob0 = new FBCachedBlob(byteArray0);
      long long0 = fBCachedBlob0.length();
      assertEquals(2L, long0);
  }

  @Test
  public void test12()  throws Throwable  {
      byte[] byteArray0 = new byte[8];
      FBCachedBlob fBCachedBlob0 = new FBCachedBlob(byteArray0);
      // Undeclared exception!
      try {
        fBCachedBlob0.getBytes((-1L), (int) (byte)0);
        fail("Expecting exception: ArrayIndexOutOfBoundsException");
      } catch(ArrayIndexOutOfBoundsException e) {
      }
  }

  @Test
  public void test13()  throws Throwable  {
      byte[] byteArray0 = new byte[7];
      FBCachedBlob fBCachedBlob0 = new FBCachedBlob(byteArray0);
      fBCachedBlob0.free();
      byte[] byteArray1 = fBCachedBlob0.getBytes((long) (byte)0, (int) (byte) (-104));
      assertNull(byteArray1);
  }

  @Test
  public void test14()  throws Throwable  {
      byte[] byteArray0 = new byte[3];
      FBCachedBlob fBCachedBlob0 = new FBCachedBlob(byteArray0);
      ByteArrayInputStream byteArrayInputStream0 = (ByteArrayInputStream)fBCachedBlob0.getBinaryStream();
      assertNotNull(byteArrayInputStream0);
      assertEquals(3L, fBCachedBlob0.length());
  }

  @Test
  public void test15()  throws Throwable  {
      byte[] byteArray0 = new byte[4];
      FBCachedBlob fBCachedBlob0 = new FBCachedBlob(byteArray0);
      fBCachedBlob0.free();
      InputStream inputStream0 = fBCachedBlob0.getBinaryStream();
      assertNull(inputStream0);
  }
}
